package com.example.database.services.impl;

import com.example.database.models.Mjesto;
import com.example.database.repositories.MjestoRepository;
import com.example.database.services.MjestoService;
import org.springframework.stereotype.Service;

@Service
public class MjestoServiceImpl implements MjestoService {

    private MjestoRepository mjestoRepository;

    public MjestoServiceImpl(MjestoRepository mjestoRepository) {
        this.mjestoRepository = mjestoRepository;
    }

    @Override
    public void saveMjesto(Mjesto zupanija) {
        mjestoRepository.save(zupanija);
    }
}
