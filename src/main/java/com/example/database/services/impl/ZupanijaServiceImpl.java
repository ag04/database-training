package com.example.database.services.impl;

import com.example.database.models.Zupanija;
import com.example.database.repositories.ZupanijaRepository;
import com.example.database.services.ZupanijaService;
import org.springframework.stereotype.Service;

@Service
public class ZupanijaServiceImpl implements ZupanijaService {

    private ZupanijaRepository zupanijaRepository;

    public ZupanijaServiceImpl(ZupanijaRepository zupanijaRepository) {
        this.zupanijaRepository = zupanijaRepository;
    }

    @Override
    public void saveZupanija(Zupanija zupanija) {
        zupanijaRepository.save(zupanija);
    }
}
