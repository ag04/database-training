package com.example.database.services;

import com.example.database.models.Zupanija;

public interface ZupanijaService {
    void saveZupanija (Zupanija zupanija);
}
