package com.example.database.services;

import com.example.database.models.Mjesto;

public interface MjestoService {
    void saveMjesto (Mjesto mjesto);
}
