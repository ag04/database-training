package com.example.database.models;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class IzvrsiteljId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "nastavnik_id")
    private Nastavnik nastavnik;

    @ManyToOne
    @JoinColumn(name = "kolegij_id")
    private Kolegij kolegij;

    @ManyToOne
    @JoinColumn(name = "uloga_izvrsitelja_id")
    private UlogaIzvrsitelja ulogaIzvrsitelja;

    public IzvrsiteljId(Nastavnik nastavnik, Kolegij kolegij, UlogaIzvrsitelja ulogaIzvrsitelja) {
        this.nastavnik = nastavnik;
        this.kolegij = kolegij;
        this.ulogaIzvrsitelja = ulogaIzvrsitelja;
    }

    public Nastavnik getNastavnik() {
        return nastavnik;
    }

    public void setNastavnik(Nastavnik nastavnik) {
        this.nastavnik = nastavnik;
    }

    public Kolegij getKolegij() {
        return kolegij;
    }

    public void setKolegij(Kolegij kolegij) {
        this.kolegij = kolegij;
    }

    public UlogaIzvrsitelja getUlogaIzvrsitelja() {
        return ulogaIzvrsitelja;
    }

    public void setUlogaIzvrsitelja(UlogaIzvrsitelja ulogaIzvrsitelja) {
        this.ulogaIzvrsitelja = ulogaIzvrsitelja;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKolegij(), getUlogaIzvrsitelja(), getNastavnik());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IzvrsiteljId)) return false;
        IzvrsiteljId that = (IzvrsiteljId) o;
        return Objects.equals(getKolegij(), that.getKolegij()) &&
                Objects.equals(getUlogaIzvrsitelja(), that.getUlogaIzvrsitelja()) &&
                Objects.equals(getNastavnik(), that.getNastavnik());
    }


}
