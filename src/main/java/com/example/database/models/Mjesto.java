package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "mjesta")
@EntityListeners(AuditingEntityListener.class)
public class Mjesto {

    @Id
    private Long postbr;

    private String nazivMjesto;

    @ManyToOne
    @JoinColumn(name = "idZupanija")
    private Zupanija zupanija;

    public Mjesto(){

    }
    public Mjesto(Long postbr, String nazivMjesto, Zupanija zupanija) {
        this.postbr = postbr;
        this.nazivMjesto = nazivMjesto;
        this.zupanija = zupanija;
    }

    public Long getPostbr() {
        return postbr;
    }

    public void setPostbr(Long postbr) {
        this.postbr = postbr;
    }

    public String getNazivMjesto() {
        return nazivMjesto;
    }

    public void setNazivMjesto(String nazivMjesto) {
        this.nazivMjesto = nazivMjesto;
    }

    public Zupanija getZupanija() {
        return zupanija;
    }

    public void setZupanija(Zupanija zupanija) {
        this.zupanija = zupanija;
    }
}
