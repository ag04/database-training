package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "izvrsitelji")
@EntityListeners(AuditingEntityListener.class)
public class Izvrsitelj {

    @EmbeddedId
    private IzvrsiteljId id;

    public Izvrsitelj(IzvrsiteljId id) {
        this.id = id;
    }
}
