package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
@Entity
@Table(name = "kolegiji")
@EntityListeners(AuditingEntityListener.class)
public class Kolegij {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private Long id;

    private String naziv;

    private String opis;

    @ManyToOne
    @JoinColumn(name = "idSmjer", nullable = false)
    private Smjer smjer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public Smjer getSmjer() {
        return smjer;
    }

    public void setSmjer(Smjer smjer) {
        this.smjer = smjer;
    }

    public Kolegij (String naziv, Smjer smjer){
        this.naziv = naziv;
        this.smjer = smjer;
    }
}
