package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "nastavnici")
@EntityListeners(AuditingEntityListener.class)
public class Nastavnik {

    @Id
    private Long jmbg;

    private String ime;

    private String prezime;

    private String adresa;

    private String titulaIspred;

    private String titulaIza;

    @ManyToOne
    @JoinColumn(name = "postbr")
    private Mjesto mjesto;

    private String lozinka;

    public Nastavnik(){

    }
}
