package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "smjerovi")
@EntityListeners(AuditingEntityListener.class)
public class Smjer {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private Long id;

    private String naziv;

    @ManyToOne
    @JoinColumn(name = "oibUstanova")
    private Ustanova ustanova;

    @ManyToOne
    @JoinColumn(name="idNadsmjer")
    private Smjer nadsmjer;

    @OneToMany(mappedBy="nadsmjer")
    private Set<Smjer> podsmjerovi = new HashSet<Smjer>();

    public Smjer(String naziv, Ustanova ustanova, Smjer nadsmjer) {
        this.naziv = naziv;
        this.ustanova = ustanova;
        this.nadsmjer = nadsmjer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Ustanova getUstanova() {
        return ustanova;
    }

    public void setUstanova(Ustanova ustanova) {
        this.ustanova = ustanova;
    }

    public Smjer getNadsmjer() {
        return nadsmjer;
    }

    public void setNadsmjer(Smjer nadsmjer) {
        this.nadsmjer = nadsmjer;
    }

    public Set<Smjer> getPodsmjerovi() {
        return podsmjerovi;
    }

    public void setPodsmjerovi(Set<Smjer> podsmjerovi) {
        this.podsmjerovi = podsmjerovi;
    }
}
