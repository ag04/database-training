package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "studenti")
@EntityListeners(AuditingEntityListener.class)
public class Student {

    @Id
    private Long jmbag;

    private String ime;

    private String prezime;

    private Date datumUpisa;

    public Student() {

    }

    @ManyToOne
    @JoinColumn(name = "postBrPrebivanje")
    private Mjesto mjestoPrebivalista;

    @ManyToOne
    @JoinColumn(name = "postBrStanovanja")
    private Mjesto mjestoStanovanja;

    @ManyToOne
    @JoinColumn(name = "smjer_id")
    private Smjer smjer;

}
