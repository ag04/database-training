package com.example.database.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "zupanije")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Zupanija implements Serializable {
    @Id
    private Long id;

    private String nazivZupanija;

    public Zupanija() {
    }

    public Zupanija(String nazivZupanija) {
        this.nazivZupanija = nazivZupanija;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazivZupanija() {
        return nazivZupanija;
    }

    public void setNazivZupanija(String nazivZupanija) {
        this.nazivZupanija = nazivZupanija;
    }
}