package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "UlogaIzvrsitelja")
@EntityListeners(AuditingEntityListener.class)
public class UlogaIzvrsitelja {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private Long id;

    private String naziv;

    public UlogaIzvrsitelja(String naziv) {
        this.naziv = naziv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }
}
