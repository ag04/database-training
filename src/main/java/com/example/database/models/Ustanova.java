package com.example.database.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ustanove")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Ustanova {
    @Id
    private Long oibUstanova;

    private String naziv;

    private String zRacun;

    private String adresa;

    private Date datumOsnutka;

    @ManyToOne
    @JoinColumn(name = "postbr")
    private Mjesto mjesto;

    public Ustanova() {
    }


}