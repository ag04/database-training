package com.example.database.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.sql.Time;

@Entity
@Table(name = "ocjene")
@EntityListeners(AuditingEntityListener.class)
public class Ocjena {

    @EmbeddedId
    private OcjenaId id;

    private Time vrijemePolaganja;

    private int vrijednost;

    public Ocjena(OcjenaId id, Time vrijemePolaganja, int vrijednost) {
        this.id = id;
        this.vrijemePolaganja = vrijemePolaganja;
        this.vrijednost = vrijednost;
    }

    public OcjenaId getId() {
        return id;
    }

    public void setId(OcjenaId id) {
        this.id = id;
    }

    public Time getVrijemePolaganja() {
        return vrijemePolaganja;
    }

    public void setVrijemePolaganja(Time vrijemePolaganja) {
        this.vrijemePolaganja = vrijemePolaganja;
    }

    public int getVrijednost() {
        return vrijednost;
    }

    public void setVrijednost(int vrijednost) {
        this.vrijednost = vrijednost;
    }
}
