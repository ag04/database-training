package com.example.database.models;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Embeddable
public class OcjenaId implements Serializable {

    @ManyToOne
    @JoinColumn(name = "jmbag_student")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "kolegij_id")
    private Kolegij kolegij;

    private Date datumPolaganja;

    public OcjenaId(Student student, Kolegij kolegij, Date datumPolaganja) {
        this.student = student;
        this.kolegij = kolegij;
        this.datumPolaganja = datumPolaganja;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Kolegij getKolegij() {
        return kolegij;
    }

    public void setKolegij(Kolegij kolegij) {
        this.kolegij = kolegij;
    }

    public Date getDatumPolaganja() {
        return datumPolaganja;
    }

    public void setDatumPolaganja(Date datumPolaganja) {
        this.datumPolaganja = datumPolaganja;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKolegij(), getDatumPolaganja(), getStudent());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OcjenaId)) return false;
        OcjenaId that = (OcjenaId) o;
        return Objects.equals(getKolegij(), that.getKolegij()) &&
                Objects.equals(getDatumPolaganja(), that.getDatumPolaganja()) &&
                Objects.equals(getStudent(), that.getStudent());
    }
}
