package com.example.database.repositories;

import com.example.database.models.Mjesto;
import org.springframework.data.repository.CrudRepository;

public interface
MjestoRepository extends CrudRepository<Mjesto, Long> {

}