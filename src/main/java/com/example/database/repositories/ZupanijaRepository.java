package com.example.database.repositories;

        import com.example.database.models.Zupanija;
        import org.springframework.data.repository.CrudRepository;

public interface ZupanijaRepository extends CrudRepository<Zupanija, Long> {

}