package com.example.database.bootstrap;
import com.example.database.services.MjestoService;
import com.example.database.services.ZupanijaService;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final ZupanijaService zupanijaService;
    private final MjestoService mjestoService;

    public DevBootstrap(ZupanijaService zupanijaService, MjestoService mjestoService) {
        this.zupanijaService = zupanijaService;
        this.mjestoService = mjestoService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();

    }

    private void initData(){
//        Zupanija zupanija1 = new Zupanija ("Grad Zagreb");
//        zupanijaService.saveZupanija(zupanija1);
//        Zupanija zupanija2 = new Zupanija ("Zagrebačka županija");
//        zupanijaService.saveZupanija(zupanija2);
//        Zupanija zupanija3 = new Zupanija ("Krapinsko-zagorska županija");
//        zupanijaService.saveZupanija(zupanija3);
//        Zupanija zupanija4 = new Zupanija ("Sisačko-moslavačka županija");
//        zupanijaService.saveZupanija(zupanija4);
//        Zupanija zupanija5 = new Zupanija ("Karlovačka županija");
//        zupanijaService.saveZupanija(zupanija5);
//        Zupanija zupanija6 = new Zupanija ("Varaždinska županija");
//        zupanijaService.saveZupanija(zupanija6);
//        Zupanija zupanija7 = new Zupanija ("Koprivničko-križevačka županija");
//        zupanijaService.saveZupanija(zupanija7);
//        Zupanija zupanija8 = new Zupanija ("Bjelovarsko-bilogorska županija");
//        zupanijaService.saveZupanija(zupanija8);
//        Zupanija zupanija9 = new Zupanija ("Primorsko-goranska  županija");
//        zupanijaService.saveZupanija(zupanija9);
//        Zupanija zupanija10 = new Zupanija ("Ličko-senjska  županija");
//        zupanijaService.saveZupanija(zupanija10);
//        Zupanija zupanija11 = new Zupanija ("Virovitičko-podravska županija");
//        zupanijaService.saveZupanija(zupanija11);
//        Zupanija zupanija12 = new Zupanija ("Požeško-slavonska županija");
//        zupanijaService.saveZupanija(zupanija12);
//        Zupanija zupanija13 = new Zupanija ("Brodsko-posavska županija");
//        zupanijaService.saveZupanija(zupanija13);
//        Zupanija zupanija14 = new Zupanija ("Zadarska županija");
//        zupanijaService.saveZupanija(zupanija14);
//        Zupanija zupanija15 = new Zupanija ("Osječko-baranjska županija");
//        zupanijaService.saveZupanija(zupanija15);
//        Zupanija zupanija16 = new Zupanija ("Šibensko-kninska županija");
//        zupanijaService.saveZupanija(zupanija16);
//        Zupanija zupanija17 = new Zupanija ("Vukovarsko-srijemska županija");
//        zupanijaService.saveZupanija(zupanija17);
//        Zupanija zupanija18 = new Zupanija ("Splitsko-dalmatinska županija");
//        zupanijaService.saveZupanija(zupanija18);
//        Zupanija zupanija19 = new Zupanija ("Istarska županija");
//        zupanijaService.saveZupanija(zupanija19);
//        Zupanija zupanija20 = new Zupanija ("Dubrovačko-neretvanska  županija");
//        zupanijaService.saveZupanija(zupanija20);
//        Zupanija zupanija21 = new Zupanija ("Međimurska županija");
//        zupanijaService.saveZupanija(zupanija21);
//
//        Mjesto mjesto1 = new Mjesto(Long.valueOf(10000), "Zagreb", zupanija1);
//        mjestoService.saveMjesto(mjesto1);
//        Mjesto mjesto2 = new Mjesto(Long.valueOf(10290), "Zaprešić", zupanija1);
//        mjestoService.saveMjesto(mjesto2);
//        Mjesto mjesto3 = new Mjesto(Long.valueOf(10295), "Kupljenovo", zupanija2);
//        mjestoService.saveMjesto(mjesto3);
//        Mjesto mjesto4 = new Mjesto(Long.valueOf(10310), "Ivanić-Grad", zupanija2);
//        mjestoService.saveMjesto(mjesto4);
//        Mjesto mjesto5 = new Mjesto(Long.valueOf(10315), "Novoselec", zupanija2);
//        mjestoService.saveMjesto(mjesto5);
//        Mjesto mjesto6 = new Mjesto(Long.valueOf(10341), "Vrbovec", zupanija2);
//        mjestoService.saveMjesto(mjesto6);
//        Mjesto mjesto7 = new Mjesto(Long.valueOf(10345), "Gradec", zupanija2);
//        mjestoService.saveMjesto(mjesto7);
//        Mjesto mjesto8 = new Mjesto(Long.valueOf(10340), "Velika Gorica", zupanija2);
//        mjestoService.saveMjesto(mjesto8);
//        Mjesto mjesto9 = new Mjesto(Long.valueOf(10340), "Gradec", zupanija2);
//        mjestoService.saveMjesto(mjesto9);
//        Mjesto mjesto10 = new Mjesto(Long.valueOf(10430), "Samobor", zupanija2);
//        mjestoService.saveMjesto(mjesto10);
//        Mjesto mjesto11 = new Mjesto(Long.valueOf(20000), "Dubrovnik", zupanija20);
//        mjestoService.saveMjesto(mjesto11);
//        Mjesto mjesto12 = new Mjesto(Long.valueOf(21000), "Split", zupanija18);
//        mjestoService.saveMjesto(mjesto12);
//        Mjesto mjesto13 = new Mjesto(Long.valueOf(21300), "Makarska", zupanija18);
//        mjestoService.saveMjesto(mjesto13);
//        Mjesto mjesto14 = new Mjesto(Long.valueOf(21465), "Jelsa", zupanija18);
//        mjestoService.saveMjesto(mjesto14);
//        Mjesto mjesto15 = new Mjesto(Long.valueOf(21480), "Vis", zupanija18);
//        mjestoService.saveMjesto(mjesto15);
//        Mjesto mjesto16 = new Mjesto(Long.valueOf(21485), "Komiža", zupanija18);
//        mjestoService.saveMjesto(mjesto16);
//        Mjesto mjesto17 = new Mjesto(Long.valueOf(22000), "Šibenik", zupanija16);
//        mjestoService.saveMjesto(mjesto17);
//        Mjesto mjesto18 = new Mjesto(Long.valueOf(22030), "Šibenik-Zablaće", zupanija16);
//        mjestoService.saveMjesto(mjesto18);
//        Mjesto mjesto19 = new Mjesto(Long.valueOf(22205), "Perković", zupanija16);
//        mjestoService.saveMjesto(mjesto19);
    }
}
